﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
namespace Almacen_Farmacorp
{
    public partial class Detalle_Producto_proveedor : MetroFramework.Forms.MetroForm
    {
        public Detalle_Producto_proveedor()
        {
            InitializeComponent();
        }

        Capanegocio.CProveedor p = new Capanegocio.CProveedor();
        Capanegocio.CProducto pr = new Capanegocio.CProducto();
        Capanegocio.Cpedidoproveedor cped = new Capanegocio.Cpedidoproveedor();
        Capacontrol.CControl CC = new Capacontrol.CControl();
        public void traerproveedor() {
            DataTable d = new DataTable();
            d = p.traerproveedor();
            cbxProveedor.DataSource = d;
            cbxProveedor.DisplayMember = "nombre_proveedor";
            cbxProveedor.ValueMember = "idproveedor";
        }
        public void traerproductos() {

            DataTable d = new DataTable();
            d = pr.traerproducto();
            dtTraerProducto.DataSource = d;
             

        }

        string idpedido;
        public void generarpedido()
        {
            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = cped.traeridpedido();

            foreach (DataRow item in d.Rows)
            {
                l.Add((DataRow)item);
            }
            l = d.AsEnumerable().ToList();
            try
            {
                if (l[0][0].ToString() != "")
                {

                    idpedido = l[0][0].ToString();
                }

                else
                {

                    idpedido = "1";
                }
            }
            catch
            {
                idpedido = "1";
            }

        }

        private void Detalle_Producto_proveedor_Load(object sender, EventArgs e)
        {
            txtfecha.Text = DateTime.Today.ToString("dd/MM/yy");
            traerproveedor();
            traerproductos();
            generarpedido();
            txtidpedido.Text = idpedido;

        }
        int r = -1;
        private void dtTraerProducto_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex==0){

                bool exist = dtDetalle_prod.Rows.Cast<DataGridViewRow>().Any(row => Convert.ToString(row.Cells["idproducto"].Value) == dtTraerProducto.CurrentRow.Cells["idProducto"].Value.ToString());


                if (!exist)
                {
                    r++;
                    dtDetalle_prod.Rows.Add();


                    dtDetalle_prod.Rows[r].Cells[1].Value = dtTraerProducto.CurrentRow.Cells["idproducto"].Value;
                    dtDetalle_prod.Rows[r].Cells[2].Value = dtTraerProducto.CurrentRow.Cells["nombre_producto"].Value;
                     

                    dtDetalle_prod.Rows[r].Cells[3].Value = dtTraerProducto.CurrentRow.Cells["cantidad_"].Value;
                    dtDetalle_prod.Rows[r].Cells[4].Value = dtTraerProducto.CurrentRow.Cells["preciounitario_"].Value;

                    try
                    {
                        for (int i = 0; i <= r; i++)
                        {

                            dtDetalle_prod.CurrentRow.Cells["subtotal"].Value = Convert.ToInt32(dtDetalle_prod.CurrentRow.Cells["cantidad"].Value.ToString()) * Convert.ToInt32(dtDetalle_prod.CurrentRow.Cells["preciounitario"].Value.ToString());
                        }
                    }
                    catch { }
                   
                }
                else {
                    if (exist)
                    {
                        try
                        {
                            if (dtDetalle_prod.CurrentRow.Cells["IdProducto"].Value.ToString() == dtTraerProducto.CurrentRow.Cells["idproducto"].Value.ToString())

                                dtDetalle_prod.CurrentRow.Cells["cantidad"].Value = Convert.ToInt32(dtDetalle_prod.CurrentRow.Cells["cantidad"].Value.ToString()) + Convert.ToInt32(dtTraerProducto.CurrentRow.Cells["cantidad_"].Value.ToString());
                            dtDetalle_prod.CurrentRow.Cells["preciounitario"].Value = dtTraerProducto.CurrentRow.Cells["preciounitario_"].Value;

                            for (int i = 0; i <= r; i++)
                            {

                                dtDetalle_prod.CurrentRow.Cells["subtotal"].Value = Convert.ToInt32(dtDetalle_prod.CurrentRow.Cells["cantidad"].Value.ToString()) * Convert.ToInt32(dtDetalle_prod.CurrentRow.Cells["preciounitario"].Value.ToString());
                            }
                        }
                        catch {
                            MessageBox.Show("seleccione la columna que desea modificar datos");
                        }
                    }
                }
            }

            else if (e.ColumnIndex == 3 || e.ColumnIndex ==4) {


                dtTraerProducto.CurrentRow.Cells["idproducto"].ReadOnly = true;
                dtTraerProducto.CurrentRow.Cells["nombre_producto"].ReadOnly = true;

            


            }

        }
        public bool datosPedido()
        {
            var tr = CC.iniTR();
            if (insertarpedido(tr))
            {
                CC.finTR(tr);
                return true;
            }
            return false;

        }
        public bool datosdetallepedido()
        {
            var tr = CC.iniTR();
            if (insertardetallepedido(tr))
            {
                CC.finTR(tr);
                return true;
            }
            return false;

        }
        public bool insertarpedido(object tr) {

            cped.v1[0] = txtidpedido.Text;
            cped.v1[1] = txtfecha.Text;
            cped.v1[2] = txtmontopedido.Text;
            cped.v1[3] = cbxEstadopedido.SelectedIndex.ToString();
            cped.v1[4] = cbxProveedor.SelectedValue.ToString();
            cped.v1[5] = "1";
 
            if(cped.insertarpedido(tr)==0){

                MessageBox.Show("error en datos de pedido");
                return false;
            }

            return true;
        }
        public bool insertardetallepedido(object tr) { 
      
        for(int i = 0; i <=r; i++){

            cped.v2[0] = dtDetalle_prod.Rows[i].Cells[3].Value.ToString();
            cped.v2[1] = dtDetalle_prod.Rows[i].Cells[4].Value.ToString();
            cped.v2[2] = dtDetalle_prod.Rows[i].Cells[5].Value.ToString();
            cped.v2[3] = txtidpedido.Text;
            cped.v2[4] = dtDetalle_prod.Rows[i].Cells[1].Value.ToString();
            if (cped.insertardetallepedido(tr) == 0) {
                MessageBox.Show("error en los datos de detalle_pedido");
                return false;
            }

        }
            
        
      
        
        return true;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (datosPedido())
            {
                datosdetallepedido();
                generarpedido();

                txtidpedido.Text = idpedido;
            }
        }
    }
}
