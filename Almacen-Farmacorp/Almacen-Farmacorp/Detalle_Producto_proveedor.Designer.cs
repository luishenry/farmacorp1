﻿namespace Almacen_Farmacorp
{
    partial class Detalle_Producto_proveedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtfecha = new System.Windows.Forms.TextBox();
            this.dtTraerProducto = new System.Windows.Forms.DataGridView();
            this.dtDetalle_prod = new System.Windows.Forms.DataGridView();
            this.txtmontopedido = new System.Windows.Forms.TextBox();
            this.cbxProveedor = new System.Windows.Forms.ComboBox();
            this.btnAgregar = new MetroFramework.Controls.MetroButton();
            this.cbxEstadopedido = new System.Windows.Forms.ComboBox();
            this.Agregar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Cantidad_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecioUnitario_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quitar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.IdProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecioUnitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Subtotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtidpedido = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtTraerProducto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDetalle_prod)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Fecha pedido";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Total monto pedido";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Estado de pedido";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Proveedor";
            // 
            // txtfecha
            // 
            this.txtfecha.Enabled = false;
            this.txtfecha.Location = new System.Drawing.Point(174, 93);
            this.txtfecha.Name = "txtfecha";
            this.txtfecha.Size = new System.Drawing.Size(100, 20);
            this.txtfecha.TabIndex = 4;
            // 
            // dtTraerProducto
            // 
            this.dtTraerProducto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtTraerProducto.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Agregar,
            this.Cantidad_,
            this.PrecioUnitario_});
            this.dtTraerProducto.Location = new System.Drawing.Point(35, 219);
            this.dtTraerProducto.Name = "dtTraerProducto";
            this.dtTraerProducto.Size = new System.Drawing.Size(637, 124);
            this.dtTraerProducto.TabIndex = 7;
            this.dtTraerProducto.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtTraerProducto_CellClick);
            // 
            // dtDetalle_prod
            // 
            this.dtDetalle_prod.AllowUserToDeleteRows = false;
            this.dtDetalle_prod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtDetalle_prod.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Quitar,
            this.IdProducto,
            this.NombreProducto,
            this.Cantidad,
            this.PrecioUnitario,
            this.Subtotal});
            this.dtDetalle_prod.Location = new System.Drawing.Point(35, 363);
            this.dtDetalle_prod.Name = "dtDetalle_prod";
            this.dtDetalle_prod.Size = new System.Drawing.Size(637, 124);
            this.dtDetalle_prod.TabIndex = 8;
            // 
            // txtmontopedido
            // 
            this.txtmontopedido.Location = new System.Drawing.Point(174, 121);
            this.txtmontopedido.Name = "txtmontopedido";
            this.txtmontopedido.Size = new System.Drawing.Size(100, 20);
            this.txtmontopedido.TabIndex = 9;
            // 
            // cbxProveedor
            // 
            this.cbxProveedor.FormattingEnabled = true;
            this.cbxProveedor.Location = new System.Drawing.Point(174, 175);
            this.cbxProveedor.Name = "cbxProveedor";
            this.cbxProveedor.Size = new System.Drawing.Size(121, 21);
            this.cbxProveedor.TabIndex = 11;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(23, 530);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(75, 23);
            this.btnAgregar.TabIndex = 12;
            this.btnAgregar.Text = "Registrar";
            this.btnAgregar.UseSelectable = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // cbxEstadopedido
            // 
            this.cbxEstadopedido.FormattingEnabled = true;
            this.cbxEstadopedido.Items.AddRange(new object[] {
            "envio en transito",
            "envio pendiende de recogida",
            "envio en reparto",
            "Destinatario Ausente",
            "En fase de devolución"});
            this.cbxEstadopedido.Location = new System.Drawing.Point(174, 148);
            this.cbxEstadopedido.Name = "cbxEstadopedido";
            this.cbxEstadopedido.Size = new System.Drawing.Size(370, 21);
            this.cbxEstadopedido.TabIndex = 10;
            // 
            // Agregar
            // 
            this.Agregar.HeaderText = "Agregar";
            this.Agregar.Name = "Agregar";
            // 
            // Cantidad_
            // 
            this.Cantidad_.HeaderText = "Cantidad_";
            this.Cantidad_.Name = "Cantidad_";
            // 
            // PrecioUnitario_
            // 
            this.PrecioUnitario_.HeaderText = "PrecioUnitario_";
            this.PrecioUnitario_.Name = "PrecioUnitario_";
            // 
            // Quitar
            // 
            this.Quitar.HeaderText = "Quitar";
            this.Quitar.Name = "Quitar";
            // 
            // IdProducto
            // 
            this.IdProducto.HeaderText = "IdProducto";
            this.IdProducto.Name = "IdProducto";
            this.IdProducto.ReadOnly = true;
            // 
            // NombreProducto
            // 
            this.NombreProducto.HeaderText = "NombreProducto";
            this.NombreProducto.Name = "NombreProducto";
            this.NombreProducto.ReadOnly = true;
            // 
            // Cantidad
            // 
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.ReadOnly = true;
            // 
            // PrecioUnitario
            // 
            this.PrecioUnitario.HeaderText = "PrecioUnitario";
            this.PrecioUnitario.Name = "PrecioUnitario";
            this.PrecioUnitario.ReadOnly = true;
            // 
            // Subtotal
            // 
            this.Subtotal.HeaderText = "Subtotal";
            this.Subtotal.Name = "Subtotal";
            this.Subtotal.ReadOnly = true;
            // 
            // txtidpedido
            // 
            this.txtidpedido.Enabled = false;
            this.txtidpedido.Location = new System.Drawing.Point(174, 64);
            this.txtidpedido.Name = "txtidpedido";
            this.txtidpedido.Size = new System.Drawing.Size(100, 20);
            this.txtidpedido.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Idpedido";
            // 
            // Detalle_Producto_proveedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1057, 648);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtidpedido);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.cbxProveedor);
            this.Controls.Add(this.cbxEstadopedido);
            this.Controls.Add(this.txtmontopedido);
            this.Controls.Add(this.dtDetalle_prod);
            this.Controls.Add(this.dtTraerProducto);
            this.Controls.Add(this.txtfecha);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Detalle_Producto_proveedor";
            this.Text = "Detalle de pedido";
            this.Load += new System.EventHandler(this.Detalle_Producto_proveedor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtTraerProducto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDetalle_prod)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtfecha;
        private System.Windows.Forms.DataGridView dtTraerProducto;
        private System.Windows.Forms.DataGridView dtDetalle_prod;
        private System.Windows.Forms.TextBox txtmontopedido;
        private System.Windows.Forms.ComboBox cbxProveedor;
        private MetroFramework.Controls.MetroButton btnAgregar;
        private System.Windows.Forms.ComboBox cbxEstadopedido;
        private System.Windows.Forms.DataGridViewButtonColumn Agregar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad_;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecioUnitario_;
        private System.Windows.Forms.DataGridViewButtonColumn Quitar;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdProducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreProducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecioUnitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Subtotal;
        private System.Windows.Forms.TextBox txtidpedido;
        private System.Windows.Forms.Label label5;
    }
}